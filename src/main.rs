mod config;
mod phys;
use phys::{AnalyticalSpectrum, Spectrum};

fn main() {
    let mut config = config::Config::init();
    config.get_args();

    if config.print_light_output {
        AnalyticalSpectrum::print_light_output(&config);
        std::process::exit(0);
    } else if config.print_stopping_power {
        AnalyticalSpectrum::print_stopping_power(&config);
        std::process::exit(0);
    }

    if config.mono > 0.0 {
        let spec_mono = Spectrum::new_mono(&config, config.mono);
        let dist = AnalyticalSpectrum::new(&config, &spec_mono);
        dist.print_dist();
        std::process::exit(0);
    }

    let spectrum = Spectrum::new(&config);
    if config.print_calculated_tdcr {
        AnalyticalSpectrum::print_calculated_tdcr(&config, &spectrum);
        std::process::exit(0);
    }

    let dist = AnalyticalSpectrum::new(&config, &spectrum);
    dist.print_dist();
}
