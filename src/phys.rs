use crate::config::Config;
extern crate rayon;
extern crate rgsl;
use rayon::prelude::*;
use rgsl::integration;
use statrs::distribution::{Binomial, Discrete, Multinomial, Poisson};
use statrs::function::erf;

#[derive(Debug)]
pub struct SpectrumColumn {
    pub energy: f64,
    pub frequency: f64,
    pub eff_energy: f64,
}

macro_rules! incl_spectra {
    ( $( $x:expr ),* ) => {
        {
            let mut profs = Vec::new();
            $(
                profs.push(($x, include_str!(concat!("../data/", $x, ".txt"))));
            )*

            profs
        }
    };
}

pub struct Spectrum {
    pub spectrum: Vec<SpectrumColumn>,
    pub max_energy: f64,
    // pub q_cdf: Vec<f64>,
    pub spec_norm: f64,
}

impl Spectrum {
    pub fn new_mono(config: &Config, energy: f64) -> Spectrum {
        let eff_energy = integral_gsl(|f| birks(f, &config), config.ion_pot..energy);
        let spec_column = SpectrumColumn {
            energy,
            frequency: 1.0,
            eff_energy,
        };
        Spectrum {
            spectrum: vec![spec_column],
            max_energy: energy,
            spec_norm: 1.0,
        }
    }

    pub fn new(config: &Config) -> Spectrum {
        let spectrum_string =
            Spectrum::add_spectra(config).unwrap_or_else(|| panic!("Spectrum not found"));
        let words_by_line = Spectrum::words_by_line(&spectrum_string);
        let mut spectrum: Vec<SpectrumColumn> = Vec::new();
        let mut energy = 0.0;
        let mut prev_energy = 0.0;
        for line in words_by_line {
            energy = line[0]
                .parse::<f64>()
                .unwrap_or_else(|_| panic!("Error in energy parsing"));
            let bin_size = match line.len() {
                4 => line[3]
                    .parse::<f64>()
                    .unwrap_or_else(|_| panic!("Error in bin size parsing")),
                3 => energy - prev_energy,
                _ => panic!("Error in spectrum file"),
            };
            // let eff_energy = integral(birks, config, config.ion_pot..energy, 1000);
            let eff_energy = integral_gsl(|f| birks(f, &config), config.ion_pot..energy);
            let frequency = line[1]
                .parse::<f64>()
                .unwrap_or_else(|_| panic!("Error in frequecy reading"));
            let spe_col = SpectrumColumn {
                energy,
                frequency: frequency * bin_size,
                eff_energy,
            };
            spectrum.push(spe_col);
            prev_energy = energy;
        }
        let spec_norm = spectrum.iter().map(|s| s.frequency).sum::<f64>();

        Spectrum {
            spectrum,
            spec_norm,
            max_energy: energy,
        }
    }

    fn add_spectra(config: &Config) -> Option<String> {
        let spectra =
            incl_spectra!("h3", "ni63", "c14", "h3_20", "ni63_20", "c14_20", "fe55", "mono");
        for spec in spectra {
            if spec.0 == config.spectrum_file.as_str() {
                return Some(spec.1.to_string());
            }
        }
        None
    }

    fn words_by_line<'a>(s: &'a str) -> Vec<Vec<&'a str>> {
        s.lines()
            .map(|line| line.split_whitespace().collect())
            .collect()
    }
}

pub struct AnalyticalSpectrum<'a> {
    spectrum: &'a Spectrum,
    config: &'a Config,
}

impl<'a> AnalyticalSpectrum<'a> {
    pub fn new(config: &'a Config, spectrum: &'a Spectrum) -> AnalyticalSpectrum<'a> {
        AnalyticalSpectrum { config, spectrum }
    }

    pub fn print_light_output(config: &Config) {
        for energy in (0..60000).step_by(50) {
            let e = energy as f64 / 1000.0; // Deposited energy in keV
            if e < config.ion_pot {
                continue;
            }
            let eff_energy = integral_gsl(|f| birks(f, &config), config.ion_pot..e);
            let avg_ph = eff_energy * config.fom + e * config.fom_d;
            // let avg_ph = eff_energy * config.fom;
            println!("{:9.5} {:9.5} {:9.5}", e, eff_energy, avg_ph);
        }
    }

    pub fn print_stopping_power(config: &Config) {
        for energy in (0..60000).step_by(5) {
            let e = energy as f64 / 1000.0; // Deposited energy in keV
            if e < config.ion_pot {
                continue;
            }
            let stop_pwr = linear_energy_transfer(e, &config);
            println!("{:9.5} {:9.5}", e, stop_pwr);
        }
    }

    pub fn print_calculated_tdcr(config: &Config, spectrum: &Spectrum) {
        let spec = AnalyticalSpectrum { config, spectrum };
        let ratios = spec.calc_ratios(config);
        println!(
            "{:.5} {:.5} {:.5} {:.5} {:.5}",
            ratios.0, ratios.1, ratios.2, ratios.3, ratios.4
        );
    }

    fn calc_ratios(self, config: &Config) -> (f64, f64, f64, f64, f64) {
        let mut eff_ab = 0.0;
        let mut eff_bc = 0.0;
        let mut eff_ac = 0.0;
        let mut eff_t = 0.0;
        let mut eff_d = 0.0;
        for specol in &self.spectrum.spectrum {
            let eff_energy = specol.eff_energy;
            let spectrum_prob = specol.frequency / self.spectrum.spec_norm;
            // let n_avg = eff_energy * config.fom;
            let e = specol.energy;
            let n_avg = eff_energy * config.fom + e * config.fom_d;
            // let n_avg = eff_energy * config.fom;
            let eff_a = 1.0 - f64::exp(-n_avg * config.pmt_efficiencies[0]);
            let eff_b = 1.0 - f64::exp(-n_avg * config.pmt_efficiencies[1]);
            let eff_c = 1.0 - f64::exp(-n_avg * config.pmt_efficiencies[2]);
            eff_ab += spectrum_prob * eff_a * eff_b;
            eff_bc += spectrum_prob * eff_b * eff_c;
            eff_ac += spectrum_prob * eff_a * eff_c;
            eff_t += spectrum_prob * eff_a * eff_b * eff_c;
            eff_d += spectrum_prob
                * (eff_a * eff_b + eff_b * eff_c + eff_a * eff_c - 2.0 * eff_a * eff_b * eff_c);
        }
        (
            eff_t / eff_ab,
            eff_t / eff_bc,
            eff_t / eff_ac,
            eff_t / eff_d,
            eff_d,
        )
    }

    pub fn print_dist(self) {
        if self.config.fom_min == 0.0 {
            println!("#Time      Prob.      Uncertainty");
            let end_time = (self.config.hist_width / self.config.time_width) as i32;
            let mut full_dist: Vec<f64> = (-end_time..=end_time)
                .into_par_iter()
                .map(|t| {
                    let bin_mid = (t as f64) * self.config.time_width;
                    let (dist_t, _) = self.total_dist(self.config.fom, bin_mid);
                    dist_t
                })
                .collect();
            for t in -end_time..=end_time {
                let bin_mid = (t as f64) * self.config.time_width;
                let dist_t = full_dist
                    .pop()
                    .unwrap_or_else(|| panic!("Error in dist popping"));
                let dist = self.config.amplitude * dist_t;
                println!("{:<10.5} {:<10.8} {:<8.5}", bin_mid, dist, 0.0);
            }
        } else {
            println!("#Ph_avg   Height    FOM");
            let mut fom = self.config.fom_min;
            loop {
                let (dist_0, avg_photons) = self.total_dist(fom, 0.0);
                let dist_0_norm = dist_0 * self.config.amplitude;
                let fom_norm = fom * self.config.amplitude;
                println!(
                    "{:<8.5} {:<10.8} {:<8.5}",
                    avg_photons, dist_0_norm, fom_norm
                );
                if fom >= self.config.fom {
                    break;
                };
                fom += self.config.fom_stp;
            }
        }
    }

    fn total_dist(&self, fom: f64, bin_mid: f64) -> (f64, f64) {
        let mut avg_photons = 0.0;
        let mut dist_0 = 0.0;
        let end_time = match self.config.avg_bin {
            true => 5,
            false => 0,
        };
        let samples = end_time * 2;
        // Average between -0.5 and 0.5 ns of the time dist.
        for t in -end_time..=end_time {
            let time = (t as f64) / 10.0 * self.config.time_width - bin_mid;
            let (dist_t, c, avg_ph) = self.spec_dist(time, fom);
            avg_photons = avg_ph;
            dist_0 += dist_t / c / (samples + 1) as f64;
        }
        (dist_0, avg_photons)
    }

    fn spec_dist(&self, time: f64, fom: f64) -> (f64, f64, f64) {
        let mut avg_photons = 0.0;
        let mut dist_t = 0.0;
        let mut c = 0.0;
        for specol in &self.spectrum.spectrum {
            let eff_energy = specol.eff_energy;
            let spectrum_prob = specol.frequency / self.spectrum.spec_norm;
            let n_avg = eff_energy * fom;
            avg_photons += n_avg * spectrum_prob;
            let (dist, norm) = self.poisson_dist(time, n_avg);
            dist_t += dist * spectrum_prob;
            c += norm * spectrum_prob;
        }
        (dist_t, c, avg_photons)
    }

    fn poisson_dist(&self, t: f64, n_avg: f64) -> (f64, f64) {
        let eff_a = self.config.pmt_efficiencies[0];
        let poisson = Poisson::new(n_avg).unwrap();
        let mut sum = 0.0;
        let mut c: f64 = 0.0;
        for n in 2..1000 {
            let p: f64 = poisson.pmf(n);
            if n < n_avg as u64 && p < 1e-3 && n > 2 {
                continue;
            }
            if n > n_avg as u64 && p < 1e-3 && n > 2 {
                break;
            }
            let binom = Binomial::new(eff_a, n).unwrap();
            for k in 1..n {
                let bin: f64 = binom.pmf(k);
                if bin < 1e-4 {
                    continue;
                };
                let dist = self.dist(t, k, n - k);
                sum += p * bin * dist;
                c += p * bin;
            }
        }
        (sum, c)
    }

    fn dist(&self, t: f64, k: u64, m: u64) -> f64 {
        let lambda = 1.0 / self.config.prompt;
        let sigma = self.config.gauss_sigma;
        let mu = self.config.centroid;

        let coef = 1.0 / (k + m) as f64;
        let kk = k as f64;
        let mm = m as f64;
        let sq2 = f64::sqrt(2.0);
        let d = coef
            * (kk * self.emg(t, mm * lambda, sq2 * sigma, mu)
                + mm * self.emg(-t, kk * lambda, sq2 * sigma, -mu));
        if !d.is_finite() {
            return 0.0;
        };
        d
    }

    fn emg(&self, t: f64, lambda: f64, sigma: f64, mu: f64) -> f64 {
        let exp_arg = lambda / 2.0 * (2.0 * mu + lambda * sigma.powf(2.0) - 2.0 * t);
        let erfc_arg = (mu + lambda * sigma.powf(2.0) - t) / f64::sqrt(2.0) / sigma;
        let emg = lambda / 2.0 * f64::exp(exp_arg) * erf::erfc(erfc_arg);
        emg
    }

    fn _multinom_dist(&self, t: f64, n_avg: f64) -> (f64, f64) {
        let poisson = Poisson::new(n_avg).unwrap();
        let mut sum = 0.0;
        let mut c: f64 = 0.0;
        for n in 2..1000 {
            let p: f64 = poisson.pmf(n);
            if n < n_avg as u64 && p < 1e-8 {
                continue;
            }
            if n > n_avg as u64 && p < 1e-8 {
                break;
            }
            let multinom = Multinomial::new(&[0.333, 0.333, 0.333], n).unwrap();
            for k in 1..n {
                for m in 1..n {
                    let l = n - k - m;
                    let multinom: f64 = multinom.pmf(&[k, m, l]);
                    let dist = self.dist(t, k, m);
                    sum += p * multinom * dist;
                    c += p * multinom;
                }
            }
        }
        (sum, c)
    }

    fn _two_over_inf_dist(&self, n_avg: f64) -> (f64, f64) {
        let eff_a = 0.5;
        let eff_b = 0.5;
        let p2 = eff_a / eff_b
            * f64::exp(-n_avg * eff_a)
            * (f64::exp(-n_avg * eff_b) + n_avg * eff_b - 1.0);
        let p_inf = (f64::exp(-n_avg) - 1.0) * eff_a - f64::exp(-n_avg * eff_a) + 1.0;
        (p2, p_inf)
    }
}

fn linear_energy_transfer(energy: f64, config: &Config) -> f64 {
    let mut eprim: f64 = 0.0;
    let mut e = energy;
    let m_e = 510.99891;

    if energy <= 0.1 {
        eprim = energy;
        e = 0.1;
    }

    let tau = e / m_e;
    let beta2 = 1.0 - (m_e / (e + m_e)).powf(2.0);

    let effe = (1.0 - beta2) * (1.0 + tau * tau / 8.0 - (2.0 * tau + 1.0) * f64::ln(2.0));
    let mut tel = 0.153536 * config.z_over_a * config.rho / beta2;
    tel *= f64::ln((e / config.ion_pot).powf(2.0)) + f64::ln(1.0 + tau / 2.0) + effe;

    if e == 0.1 {
        tel *= eprim / 0.1
    }
    tel
}

pub fn birks(energy: f64, config: &Config) -> f64 {
    // (1.0 - config.ratio)
    //     / (1.0 + config.kb * f64::powf(linear_energy_transfer(energy, config), config.misc))
    //     + config.ratio
    (1.0 - config.ratio) / (1.0 + config.kb * linear_energy_transfer(energy, config)) + config.ratio
}

// pub fn birks(energy: f64, config: &Config) -> f64 {
//     // (1.0 - config.ratio) / (1.0 + config.kb * linear_energy_transfer(energy, config)) + config.ratio
//     (1.0 - config.ratio) * f64::exp(-config.kb * linear_energy_transfer(energy, config))
//         + config.ratio
// }

pub fn integral_gsl<F>(f: F, range: std::ops::Range<f64>) -> f64
where
    F: Fn(f64) -> f64,
{
    let val;
    val = integration::qng(f, range.start, range.end, 1e-4, 0.01);
    return val.1;
}

pub fn _integral<F>(f: F, config: &Config, range: std::ops::Range<f64>, n_steps: u32) -> f64
where
    F: Fn(f64, &Config) -> f64,
{
    if range.end < config.ion_pot {
        return 0.0000001;
    }
    let step_size = (range.end - range.start) / n_steps as f64;

    let mut integral = (f(range.start, config) + f(range.end, config)) / 2.;
    let mut pos = range.start + step_size;
    while pos < range.end {
        integral += f(pos, config);
        pos += step_size;
    }
    integral * step_size
}
