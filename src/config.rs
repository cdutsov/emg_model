extern crate argparse;
use argparse::{ArgumentParser, List, Store, StoreTrue};

pub struct Config {
    pub spectrum_file: String,
    pub fom: f64,
    pub fom_d: f64,
    pub time_width: f64,
    pub hist_width: f64,
    pub fom_min: f64,
    pub fom_stp: f64,
    pub prompt: f64,
    pub gauss_sigma: f64,
    pub centroid: f64,
    pub z_over_a: f64,
    pub rho: f64,
    pub ion_pot: f64,
    pub kb: f64,
    pub pmt_efficiencies: Vec<f64>,
    pub avg_bin: bool,
    pub print_light_output: bool,
    pub print_stopping_power: bool,
    pub print_calculated_tdcr: bool,
    pub ratio: f64,
    pub amplitude: f64,
    pub misc: f64,
    pub mono: f64,
}

impl Config {
    pub fn init() -> Config {
        Config {
            avg_bin: false,
            fom_min: 0.0,
            fom: 1.0,
            fom_d: 0.0,
            fom_stp: 0.02,
            time_width: 1.0,
            hist_width: 40.0,
            spectrum_file: "".to_string(),
            prompt: 0.0,
            centroid: 0.0,
            gauss_sigma: 1e-6,
            z_over_a: 0.5459,
            rho: 0.98,
            ion_pot: 0.06457,
            kb: 0.010,
            print_light_output: false,
            pmt_efficiencies: vec![0.5, 0.5, 0.0],
            print_stopping_power: false,
            print_calculated_tdcr: false,
            ratio: 0.0,
            amplitude: 1.0,
            misc: 0.0,
            mono: 0.0,
        }
    }

    pub fn get_args(&mut self) {
        let mut pmt_efficiencies = vec![0.5, 0.5, 0.0];
        {
            let mut ap = ArgumentParser::new();
            ap.set_description("Height of time distribution versus FOM");
            ap.refer(&mut self.fom).add_option(
                &["--fom", "-f"],
                Store,
                "Figure of merit, photoelectrons per keV",
            );
            ap.refer(&mut self.fom_d).add_option(
                &["--fom-d", "-F"],
                Store,
                "Figure of merit delayed, ph.e per keV",
            );
            ap.refer(&mut self.fom_min)
                .add_option(&["--fom-min", "-m"], Store, "Figure of merit");
            ap.refer(&mut self.kb).add_option(
                &["--kB"],
                Store,
                "kB value in cm/MeV, Default: 0.010",
            );
            ap.refer(&mut self.z_over_a).add_option(
                &["--za"],
                Store,
                "Z/A value of the cocktail, Default: 0.543 (UG)",
            );
            ap.refer(&mut self.rho).add_option(
                &["--rho"],
                Store,
                "Density of the cocktail g/cm3, Default: 0.8669 (UG)",
            );
            ap.refer(&mut self.ion_pot).add_option(
                &["--ion-pot"],
                Store,
                "Ionization potential the cocktail keV, Default: 0.06457 (UG)",
            );
            ap.refer(&mut self.fom_stp)
                .add_option(&["--step"], Store, "Figure of merit step size");
            ap.refer(&mut self.time_width)
                .add_option(&["--width", "-w"], Store, "Bin width in ns");
            ap.refer(&mut self.hist_width).add_option(
                &["--hist-width", "-H"],
                Store,
                "Bin width in ns",
            );
            ap.refer(&mut self.avg_bin).add_option(
                &["--avg-bin", "-a"],
                StoreTrue,
                "Give average value for bin",
            );
            ap.refer(&mut self.print_light_output).add_option(
                &["--light-output"],
                StoreTrue,
                "Print cocktail light output (Birks Equation)",
            );
            ap.refer(&mut self.print_calculated_tdcr).add_option(
                &["--tdcr"],
                StoreTrue,
                "Print TDCR",
            );
            ap.refer(&mut self.print_stopping_power).add_option(
                &["--stopping-power"],
                StoreTrue,
                "Print stopping power (Bethe formula)",
            );
            ap.refer(&mut self.spectrum_file).add_option(
                &["--spec", "-s"],
                Store,
                "Spectrum 2 columns file",
            );
            ap.refer(&mut self.prompt).add_option(
                &["--prompt", "-p"],
                Store,
                "Prompt lambda, 1/ns",
            );
            ap.refer(&mut self.misc)
                .add_option(&["--misc"], Store, "Miscellaneous parameter");
            ap.refer(&mut self.mono).add_option(
                &["--mono"],
                Store,
                "Monoenergetic electron energy",
            );
            ap.refer(&mut self.ratio).add_option(
                &["--ratio", "-r"],
                Store,
                "Delayed to prompt ratio, (default 0.0)",
            );
            ap.refer(&mut self.gauss_sigma).add_option(
                &["--sigma", "-g"],
                Store,
                "Gaussian sigma, ns",
            );
            ap.refer(&mut self.centroid).add_option(
                &["--center", "-c"],
                Store,
                "Centroid position, ns",
            );
            ap.refer(&mut pmt_efficiencies).add_option(
                &["--pmt-eff", "-E"],
                List,
                "Relative efficiencies of PMTs A and B",
            );
            ap.refer(&mut self.amplitude).add_option(
                &["--amp", "-A"],
                Store,
                "Multiply the whole distribution by a number, Default: 1.0",
            );
            ap.parse_args_or_exit();
        }
        self.pmt_efficiencies = pmt_efficiencies;
    }
}
